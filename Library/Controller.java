package sample.kursTemp;

import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.Pane;

import java.sql.SQLException;

public class Controller {
    ObservableList<Library> libraryData = Const.LIBRARY_DATA;

    @FXML
    private TableView<Library> base_table;

    @FXML
    private TableColumn<Library, Integer> article_table;

    @FXML
    private TableColumn<Library, String> author_table;

    @FXML
    private TableColumn<Library, String> name_table;

    @FXML
    private TableColumn<Library, Integer> inStock_table;

    @FXML
    private TableColumn<Library, Integer> issued_table;

    @FXML
    private TableColumn<Library, Integer> ordered_table;

    @FXML
    private TableColumn<Library, String> redactor_table;

    @FXML
    private TableColumn<Library, Integer> redactor_year_table;

    @FXML
    private TextField find_unit_field;

    @FXML
    private ComboBox<?> find_unit;

    @FXML
    private Label error_find;

    @FXML
    private TextField add_author;

    @FXML
    private TextField add_name;

    @FXML
    private TextField add_in_stock;

    @FXML
    private Label error_add;

    @FXML
    private TextField add_article;

    @FXML
    private TextField add_redactor;

    @FXML
    private TextField add_redactor_year;

    @FXML
    private TextField edit_article;

    @FXML
    private Pane edit_panel;

    @FXML
    private TextField edit_author;

    @FXML
    private TextField edit_name;

    @FXML
    private TextField edit_issued;

    @FXML
    private Button save_button;

    @FXML
    private Label save_error;

    @FXML
    private Label save_success;

    @FXML
    private TextField edit_in_stock;

    @FXML
    private TextField edit_redactor;

    @FXML
    private TextField edit_redactor_year;

    @FXML
    private TextField del_article;

    @FXML
    private Label del_success;

    @FXML
    private Label del_error;

    @FXML
    private Label error_export;

    @FXML
    private Label export_success;

    @FXML
    private TextField in_stock_article;

    @FXML
    private Label in_stock_article_error;

    @FXML
    private Pane in_stock_panel;

    @FXML
    private TextField in_stock_add;

    @FXML
    private Label in_stock_error;

    @FXML
    private Label in_stock_success;

    @FXML
    private TextField issued_add;

    @FXML
    private TextField ordered_add;

    @FXML
    private TextField delete_add;

    @FXML
    private Label error_add_article;

    @FXML
    private TextField export_path;

    @FXML
    private TextField export_name;


    @FXML
    private void initialize() {
        edit_panel.setVisible(false);
        in_stock_panel.setVisible(false);

        setVisibleFalse(in_stock_error);
        setVisibleFalse(in_stock_article_error);
        setVisibleFalse(del_error);
        setVisibleFalse(save_error);
        setVisibleFalse(in_stock_success);
        setVisibleFalse(save_success);
        setVisibleFalse(del_success);

        // устанавливаем тип и значение которое должно хранится в колонке
        article_table.setCellValueFactory(new PropertyValueFactory<>("article"));
        name_table.setCellValueFactory(new PropertyValueFactory<>("name"));
        author_table.setCellValueFactory(new PropertyValueFactory<>("author"));
        ordered_table.setCellValueFactory(new PropertyValueFactory<>("ordered"));
        inStock_table.setCellValueFactory(new PropertyValueFactory<>("inStock"));
        issued_table.setCellValueFactory(new PropertyValueFactory<>("issued"));
        redactor_table.setCellValueFactory(new PropertyValueFactory<>("redactor"));
        redactor_year_table.setCellValueFactory(new PropertyValueFactory<>("year"));
        showDataBase();
    }


    /**
     * При нажатии кнопки добавляет данные в бд
     */
    @FXML
    public void onAdd() throws SQLException, ClassNotFoundException {
        error_add.setVisible(false); //скрывает текст ошибки

        if (isEmptyField(add_author, error_add) && isIntZero(add_article, error_add)
                && isEmptyField(add_name, error_add) && isEmptyField(add_in_stock, error_add)
                && isInt(add_in_stock, error_add) && isInt(add_redactor_year, error_add)
                && isEmptyField(add_redactor_year, error_add) && isEmptyField(add_redactor, error_add)) {//проверяет правильность и заполненность полей

            //добавляет данные если артикль уникальный
            if (isUnique(add_article)) {
                Library library = new Library();
                library.setArticle(Integer.parseInt(add_article.getText()));
                library.setName(add_name.getText());
                library.setInStock(Integer.parseInt(add_in_stock.getText()));
                library.setAuthor(add_author.getText());
                library.setRedactor(add_redactor.getText());
                library.setYear(Integer.parseInt(add_redactor_year.getText()));
                Const.LIBRARY_DATA.add(library);

                DatabaseHandler databaseHandler = new DatabaseHandler();
                databaseHandler.setStore(library);
            }
            base_table.refresh();
            showDataBase();
        } else {
            error_add.setVisible(true); //отображает текст ошибки
        }

    }

    /**
     * При нажатии кнопки удаляет данные из бд по артиклю
     */
    @FXML
    public void onDelete() {
        del_error.setVisible(false); //скрывает текст при ошибке выполнения команды
        del_success.setVisible(false); //отображает текст при успешном выполнении команды

        try {
            if (isEmptyField(del_article, del_error) && isIntZero(del_article, del_error)
                    && !isUnique(del_article)) { //проверяет правильность и заполненность поля артикля
                DatabaseHandler databaseHandler = new DatabaseHandler();
                databaseHandler.delete(Integer.parseInt(del_article.getText()));

                //обновление данных бд для отображения
                base_table.refresh();
                showDataBase();

                del_success.setVisible(true); //отображает текст при успешном выполнении команды
            } else {
                del_error.setVisible(true); //отображает текст при ошибке выполнения команды
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * При нажатии кнопки отображает панель для редактирования
     */
    @FXML
    public void onEdit() {
        setVisibleFalse(save_error);  //скрывает текст при ошибке выполнения команды
        save_button.setDisable(false); //включает кнопку сохранения при редактировании

        if (isEmptyField(edit_article, error_add_article) && !isUnique(edit_article)
                && isIntZero(edit_article, error_add_article)) { //проверяет правильность и заполненность поля артикля
            edit_panel.setVisible(true); //отображает панель редактирования при успешном выполнении команды
            setVisibleFalse(save_success); //скрывает текст успешного выполнения
            setVisibleFalse(save_error); //скрывает текст ошибки

            for (Library library : libraryData) {
                if (library.getArticle() == Integer.parseInt(edit_article.getText())) {
                    edit_author.setText(library.getAuthor());
                    edit_name.setText(library.getName());
                    edit_in_stock.setText(String.valueOf(library.getInStock()));
                    edit_issued.setText(String.valueOf(library.getIssued()));
                    edit_redactor.setText(String.valueOf(library.getRedactor()));
                    edit_redactor_year.setText(String.valueOf(library.getYear()));
                }
            }

        } else {
            setVisibleTrue(error_add_article); //отображает текст при ошибке выполнения команды
            edit_panel.setVisible(false); //убирает видимость панели
        }
    }

    /**
     * Сохраняет введенные пользователем данные в бд
     */
    @FXML
    public void onSave() throws Exception {
        setVisibleFalse(save_error); //скрывает текст ошибки
        setVisibleFalse(save_success); //скрывает текст успешного выполнения

        if (isEmptyField(edit_name, save_error) && isEmptyField(edit_in_stock, save_error)
                && isInt(edit_in_stock, save_error) && isEmptyField(edit_author, save_error)) { //проверяет правильность и заполненность полей
            Library library = new Library();
            library.setArticle(Integer.parseInt(edit_article.getText()));
            library.setName(edit_name.getText());
            library.setInStock(Integer.parseInt(edit_in_stock.getText()));
            library.setAuthor(edit_author.getText());
            library.setIssued(Integer.parseInt(edit_issued.getText()));
            library.setRedactor(edit_redactor.getText());
            library.setYear(Integer.parseInt(edit_redactor_year.getText()));

            DatabaseHandler databaseHandler = new DatabaseHandler();
            databaseHandler.updateStore(library);

            base_table.refresh();
            showDataBase();

            setVisibleTrue(save_success); //отображает текст при успешном выполнении

            save_button.setDisable(true); //выключает кнопку сохранения при редактировании
        } else {
            setVisibleTrue(save_error); //отображает текст при ошибке выполнении
        }

    }

    /**
     * При нажатии кнопки отображает панель для учета
     */
    @FXML
    public void onEditInStock() {
        setVisibleFalse(in_stock_article_error);  //скрывает текст при ошибке выполнения команды
        if (isEmptyField(in_stock_article, in_stock_article_error) && !isUnique(in_stock_article)
                && isIntZero(in_stock_article, in_stock_article_error)) { //проверяет правильность и заполненность поля артикля
            in_stock_panel.setVisible(true); //отображает панель редактирования при успешном выполнении команды
            setVisibleFalse(in_stock_success); //скрывает текст успешного выполнения
            setVisibleFalse(in_stock_error); //скрывает текст ошибки
        } else {
            setVisibleTrue(in_stock_article_error); //отображает текст при ошибке выполнения команды
            in_stock_panel.setVisible(false); //убирает видимость панели
        }

    }

    /**
     * Добавляет данные из учета в бд
     */
    @FXML
    public void onAddInStock() {
        setVisibleFalse(in_stock_error); //скрывает текст ошибки
        setVisibleFalse(in_stock_success); //скрывает текст успешного выполнения

        int ordered = emptyNotZero(ordered_add);
        int inStock = emptyNotZero(in_stock_add);
        int issued = emptyNotZero(issued_add);
        int decommission = emptyNotZero(delete_add);

        try {
            DatabaseHandler databaseHandler = new DatabaseHandler();
            int article = Integer.parseInt(in_stock_article.getText());

            for (Library library : libraryData) {
                if (library.getArticle() == article) {
                    ordered += library.getOrdered() - emptyNotZero(in_stock_add); //заказано
                    inStock += library.getInStock() - issued - decommission; //в наличии
                    issued += library.getIssued(); //выдано

                    databaseHandler.updatePartOfStore(Const.LIBRARY_ORDERED, ordered, article);
                    databaseHandler.updatePartOfStore(Const.LIBRARY_IN_STOCK, inStock, article);
                    databaseHandler.updatePartOfStore(Const.LIBRARY_ISSUED, issued, article);

                    base_table.refresh();
                    showDataBase();
                }
            }
            issued_add.clear();
            in_stock_add.clear();
            delete_add.clear();
            ordered_add.clear();
            setVisibleTrue(in_stock_success); //отображает текст при успешном выполнения
        } catch (Exception e) {
            setVisibleTrue(in_stock_error); //отображает текст при ошибке выполнении
        }
    }

    @FXML
    void onExport() {
        setVisibleFalse(error_export); //скрывает текст ошибки
        setVisibleFalse(export_success); //скрывает текст успешного выполнения
        try {
            DatabaseHandler databaseHandler = new DatabaseHandler();
            databaseHandler.outPut(export_path.getText() + export_name.getText());
            setVisibleTrue(export_success);
        } catch (Exception e) {
            e.printStackTrace();
            setVisibleTrue(error_export);
        }
    }

    /**
     * Находит книги по фильтру
     */
    @FXML
    public void onFind() {
        DatabaseHandler databaseHandler = new DatabaseHandler();
        libraryData.clear();
        String unitFind = find_unit.getValue().toString();
        String search = find_unit_field.getText();
        ObservableList<Library> list;
        switch (unitFind) {
            case ("артиклю"):
                list = databaseHandler.searchInt("article", Integer.parseInt(search));
                if (list.isEmpty()) {
                    setVisibleTrue(error_find);
                } else {
                    base_table.setItems(list);
                }
                break;
            case ("автору"):
                list = databaseHandler.searchString("author", search);
                if (list.isEmpty()) {
                    setVisibleTrue(error_find);
                } else {
                    base_table.setItems(list);
                }
                break;
            case ("названию"):
                list = databaseHandler.searchString("name_table", search);
                if (list.isEmpty()) {
                    setVisibleTrue(error_find);
                } else {
                    base_table.setItems(list);
                }
                break;
        }
    }

    /**
     * Возвращает отображение всей бд
     */
    @FXML
    public void onBack() {
        libraryData.clear();
        showDataBase();
    }


    private int emptyNotZero(TextField textField) {
        String field = textField.getText();
        if (field.isEmpty()) {
            return 0;
        }
        return Integer.parseInt(field);
    }

    /**
     * Проверяет текстовое поле на наличие введенных данных
     *
     * @param textField текстовое поле для проверки
     * @return false - поле пустое, true - поле не пустое
     */
    private boolean isEmptyField(TextField textField, Label label) {
        String text = textField.getText().replaceAll("^ +", "");
        textField.setText(text);
        if (text.isEmpty()) {
            label.setVisible(true);
            return false;
        }
        return true;
    }

    /**
     * Проверяет текстовое поле на правильность ввода целого числа (может быть равно нулю)
     *
     * @param textField текстовое поле для проверки
     * @return false - в поле не целое число, true - в поле целое число
     */
    private boolean isInt(TextField textField, Label label) {
        try {
            int intText = Integer.parseInt(textField.getText().replaceAll(" +", ""));
            if (intText >= 0) {
                textField.setText(Integer.toString(intText));
                return true;
            }
        } catch (NumberFormatException e) {
            label.setVisible(true);
            return false;
        }
        return false;
    }

    /**
     * Проверяет текстовое поле на правильность ввода целого числа (не может быть равно нулю)
     *
     * @param textField текстовое поле для проверки
     * @return false - в поле не целое число, true - в поле целое число
     */
    private boolean isIntZero(TextField textField, Label label) {
        try {
            int intText = Integer.parseInt(textField.getText().replaceAll(" +", ""));
            if (intText > 0) {
                textField.setText(Integer.toString(intText));
                return true;
            }
        } catch (NumberFormatException e) {
            label.setVisible(true);
            return false;
        }
        return false;
    }


    /**
     * Проверяет уникальность введенного артикля
     *
     * @return false -  поле не уникальное,  true - поле уникальное
     */
    public boolean isUnique(TextField textField) {
        for (Library library : libraryData) {
            if (library.getArticle() == Integer.parseInt(textField.getText())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Получает данные из бд для демонстрации
     */
    private void showDataBase() {
        DatabaseHandler databaseHandler = new DatabaseHandler();
        libraryData = databaseHandler.get();
        base_table.setItems(libraryData);
    }


    public void setVisibleTrue(Label label) {
        label.setVisible(true);
    }

    public void setVisibleFalse(Label label) {
        label.setVisible(false);
    }


}

