package sample.kursTemp;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.*;

/*
 * Класс для представления работы с запросами бд
 */
public class DatabaseHandler extends Configs {
    Connection dbConnection;

    /**
     * Подключается к бд
     *
     * @return соединение с бд
     */
    public Connection getDbConnection() throws ClassNotFoundException, SQLException {
        String connectionString = "jdbc:mysql://" + dbHost + ":" + dbPort + "/" + dbName + "?autoReconnect=true&useSSL=false&serverTimezone=Europe/London";

        Class.forName("com.mysql.cj.jdbc.Driver");

        dbConnection = DriverManager.getConnection(connectionString, dbUser, dbPass);

        return dbConnection;
    }

    /**
     * Обновляет поле с целым числом в бд
     *
     * @param cons    колонка для добавления
     * @param value   значение для обновления
     * @param article артикль для проверки
     */
    public void updatePartOfStore(String cons, int value, int article) throws SQLException, ClassNotFoundException {
        PreparedStatement inserts = getDbConnection().prepareStatement(" UPDATE " + Const.LIBRARY
                + " SET " + cons + " = " + value + " WHERE " + Const.LIBRARY_ARTICLE + " = " + article + " ;"); //запрос для обновления данных в бд

        inserts.executeUpdate();
    }

    /**
     * Обновляет данные в бд
     *
     * @param library параметры класса Store(склад) для обновления
     */
    public void updateStore(Library library) throws SQLException, ClassNotFoundException {
        PreparedStatement inserts = getDbConnection().prepareStatement(" UPDATE " + Const.LIBRARY
                + " SET " + Const.LIBRARY_AUTHOR + " =  ?, " + Const.LIBRARY_NAME + " = ?, "
                + Const.LIBRARY_IN_STOCK + " =  ?, " + Const.LIBRARY_ISSUED + " =  ? "
                + Const.LIBRARY_REDACTOR + " =  ? " + Const.LIBRARY_REDACTOR_YEAR + " =  ? "
                + " WHERE " + Const.LIBRARY_ARTICLE + "= ? ; ");//запрос для обновления данных в бд

        inserts.setString(1, library.getAuthor());
        inserts.setString(2, library.getName());
        inserts.setInt(3, library.getInStock());
        inserts.setInt(4, library.getIssued());
        inserts.setString(5, library.getRedactor());
        inserts.setInt(6, library.getYear());
        inserts.setInt(7, library.getArticle());
        inserts.executeUpdate();
    }

    /**
     * Добавляет данные в бд
     *
     * @param library параметры класса Library(библиотека) для добавления
     */
    public void setStore(Library library) throws SQLException, ClassNotFoundException {
        PreparedStatement inserts = getDbConnection().prepareStatement(" INSERT INTO " +
                Const.LIBRARY + " ( " + Const.LIBRARY_ARTICLE + ", " + Const.LIBRARY_AUTHOR + ", "
                + Const.LIBRARY_NAME + ", " + Const.LIBRARY_IN_STOCK + ", " + Const.LIBRARY_ISSUED + ","
                + Const.LIBRARY_ORDERED + ", " + Const.LIBRARY_REDACTOR + ", " + Const.LIBRARY_REDACTOR_YEAR + " )"
                + " VALUES (?, ?, ?, ?, ?, ?, ?, ?);"); //запрос для добавления данных в бд

        inserts.setInt(1, library.getArticle());
        inserts.setString(2, library.getAuthor());
        inserts.setString(3, library.getName());
        inserts.setInt(4, library.getInStock());
        inserts.setInt(5, library.getIssued());
        inserts.setInt(6, library.getOrdered());
        inserts.setString(7, library.getRedactor());
        inserts.setInt(8, library.getYear());
        inserts.executeUpdate();
    }


    /**
     * Получает данные из бд
     *
     * @return лист данных из бд
     */
    public ObservableList<Library> get() {
        try {
            //подключение к бд
            Connection conn = getDbConnection();
            //создание запроса к бд
            PreparedStatement statement = conn.prepareStatement(
                    "SELECT * FROM library ");

            ResultSet result = statement.executeQuery();
            ObservableList<Library> list = FXCollections.observableArrayList();
            //получение данных из бд
            while (result.next()) {
                Library library = new Library();
                library.setArticle(result.getInt("article"));
                library.setAuthor(result.getString("author"));
                library.setName(result.getString("name_table"));
                library.setOrdered(result.getInt("ordered"));
                library.setInStock(result.getInt("in_stock"));
                library.setIssued(result.getInt("issued"));
                library.setIssued(result.getInt("redactor"));
                library.setIssued(result.getInt("year"));
                list.add(library);
            }
            System.out.println("Все хорошо");
            return list;
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return null;
    }

    /**
     * Находит совпадения строкового типа по фильтру
     *
     * @param type   параметр для обозначения стобца поиска в бд
     * @param search то, что нужно найти
     * @return массив совпадений
     */
    public ObservableList<Library> searchString(String type, String search) {
        ObservableList<Library> list = FXCollections.observableArrayList();
        try {
            DatabaseHandler databaseHandler = new DatabaseHandler();
            //подключение к бд
            Connection conn = databaseHandler.getDbConnection();
            //создание запроса к бд
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM library WHERE " + type + " = ?;");
            ps.setString(1, search);
            ResultSet result = ps.executeQuery();

            //получение данных из бд
            while (result.next()) {
                Library library = new Library();
                library.setArticle(result.getInt("article"));
                library.setAuthor(result.getString("author"));
                library.setName(result.getString("name_table"));
                library.setOrdered(result.getInt("ordered"));
                library.setInStock(result.getInt("in_stock"));
                library.setIssued(result.getInt("issued"));
                library.setIssued(result.getInt("redactor"));
                library.setIssued(result.getInt("year"));
                list.add(library);
            }

        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    /**
     * Находит совпадения числового целого типа по фильтру
     *
     * @param type   параметр для обозначения стобца поиска в бд
     * @param search то, что нужно найти
     * @return массив совпадений
     */
    public ObservableList<Library> searchInt(String type, int search) {
        ObservableList<Library> list = FXCollections.observableArrayList();
        try {
            DatabaseHandler databaseHandler = new DatabaseHandler();
            //подключение к бд
            Connection conn = databaseHandler.getDbConnection();
            //создание запроса к бд
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM library WHERE " + type + " = ?;");
            ps.setInt(1, search);
            ResultSet result = ps.executeQuery();

            //получение данных из бд
            while (result.next()) {
                Library library = new Library();
                library.setArticle(result.getInt("article"));
                library.setAuthor(result.getString("author"));
                library.setName(result.getString("name_table"));
                library.setOrdered(result.getInt("ordered"));
                library.setInStock(result.getInt("in_stock"));
                library.setIssued(result.getInt("issued"));
                library.setIssued(result.getInt("redactor"));
                library.setIssued(result.getInt("year"));
                list.add(library);
            }

        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return list;
    }

    /**
     * Экспортирует данные из бд
     *
     * @param path полный путь к файлу (месту сохраения)
     */
    public void outPut(String path) {
        try {
            //подключение к бд
            Connection conn = getDbConnection();
            //создание запроса к бд
            PreparedStatement ps = conn.prepareStatement("SELECT * FROM library INTO OUTFILE " + path);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }

    /**
     * Удаляет книгу из бд по артиклю
     *
     * @param del_article значение артикля для удаления
     */
    public void delete(int del_article) {
        try {
            //подключение к бд
            Connection conn = getDbConnection();
            //создание запроса к бд
            PreparedStatement ps = conn.prepareStatement("DELETE FROM library WHERE article = ?;");
            ps.setInt(1, del_article);
            ps.executeUpdate();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }

}