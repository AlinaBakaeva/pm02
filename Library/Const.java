package sample.kursTemp;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/*
 * Класс для представления констант
 */
public class Const extends Configs {
    public static final String LIBRARY = "library";

    public static final String LIBRARY_ARTICLE = "article";
    public static final String LIBRARY_AUTHOR = "author";
    public static final String LIBRARY_NAME = "name_table";
    public static final String LIBRARY_IN_STOCK = "in_stock";
    public static final String LIBRARY_ISSUED = "issued";
    public static final String LIBRARY_ORDERED = "ordered";
    public static final String LIBRARY_REDACTOR = "redactor";
    public static final String LIBRARY_REDACTOR_YEAR = "year";


    public static ObservableList<Library> LIBRARY_DATA = FXCollections.observableArrayList();

}
