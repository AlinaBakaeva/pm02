package sample.kursTemp;

public class Library {
    private int article;
    private String author;
    private String name;
    private int inStock;
    private int issued;
    private int ordered;
    private int year;
    private String redactor;

    public Library(int article, String author, String name, int inStock, int issued, int ordered) {
        this.article = article;
        this.author = author;
        this.name = name;
        this.inStock = inStock;
        this.issued = issued;
        this.ordered = ordered;
    }
    public Library(int article, String author, String name, int inStock) {
        this.article = article;
        this.author = author;
        this.name = name;
        this.inStock = inStock;
    }

    public Library(int article, String author, String name, int inStock, int issued, int ordered, int year, String redactor) {
        this.article = article;
        this.author = author;
        this.name = name;
        this.inStock = inStock;
        this.issued = issued;
        this.ordered = ordered;
        this.year = year;
        this.redactor = redactor;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getRedactor() {
        return redactor;
    }

    public void setRedactor(String redactor) {
        this.redactor = redactor;
    }

    public Library() {

    }

    public int getArticle() {
        return article;
    }

    public void setArticle(int article) {
        this.article = article;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInStock() {
        return inStock;
    }

    public void setInStock(int inStock) {
        this.inStock = inStock;
    }

    public int getIssued() {
        return issued;
    }

    public void setIssued(int issued) {
        this.issued = issued;
    }

    public int getOrdered() {
        return ordered;
    }

    public void setOrdered(int ordered) {
        this.ordered = ordered;
    }
}
